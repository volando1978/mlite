﻿using UnityEngine;
using System.Collections;

public class disparo : MonoBehaviour
{


		public int shootTime;
		public int shotTimeNormal;
		public int shotTimewBock;
		public int shotTimeLaser;
		public int shotTimeMoire;

		public int shotTimeTWay;
		public int shotTimeTri;
		public int shotTimeCircular;


//		public int fireCounter;
		public int largo;
		public int tLaser;

//	[SerializeField]
		public  bool isLaserState;

//		public int speed;
		public float shotForce;
		public GameObject rayo;



		public void dispara (enemyController _enemyController, GameObject bullet, GameObject bulletThin, GameObject cirController, GameObject bulletLaser, GameObject raycastObj)
		{
				GameObject closest = _enemyController.getClosest (transform.position);

//				if (Input.GetKey ("space")) {

				for (int input = 0; input < 1; ++input) {
						Time.timeScale = 1;
//						WeaponsController.setWeaponOrder ();
						if (closest) {
								if (shootTime > 1) {
			
										shootTime -= 1;
						
			
								} else if (isBullets ()) {
										switch (WeaponsController.currentWeapon) { 
				
										case WeaponsController.WEAPONS.WBLOCK:
				
												float distanceToGun = GetComponentInChildren<SpriteRenderer> ().sprite.bounds.size.x / 2;
												Vector2 shootPos = new Vector2 (transform.position.x + distanceToGun, transform.position.y);

												GameObject bulletw = Instantiate (bullet, shootPos, transform.rotation) as GameObject;
												bulletw.GetComponent<Rigidbody> ().AddForce (transform.right * shotForce);
												Sound.playwBulletClip ();

												shootTime = shotTimewBock;
												WeaponsController.bullets [(int)WeaponsController.currentWeapon] -= 1;
												Camera.main.GetComponent<cameraScript> ().StartCoroutine ("shakeSmall", 1f);

												break;
				
										case WeaponsController.WEAPONS.LASER:
												GameObject bulletL = Instantiate (bulletLaser, transform.position, transform.rotation) as GameObject;
												bulletL.GetComponent<Rigidbody> ().AddForce (transform.right * shotForce / 80);
//												bulletL.GetComponent<LineRenderer> ().SetWidth (0.02f, 0.02f);
												Sound.playlaserBulletClip ();
					

//												GameObject ray = Instantiate (rayo, transform.position, transform.rotation) as GameObject;
//												ray.GetComponent<RayoScr> ().target = GetComponent<playerMovement> ().closest;
//												ray.GetComponent<RayoScr> ().timer = 5;

//												isLaserState = true;
												GameObject raycastL = Instantiate (raycastObj, transform.position, transform.rotation) as GameObject;
												raycastL.transform.parent = transform;
												raycastL.GetComponent<RaycastScr> ().length = globales.SCREENW;

												WeaponsController.bullets [(int)WeaponsController.currentWeapon] -= 1;
												shootTime = shotTimeLaser;
												Camera.main.GetComponent<cameraScript> ().StartCoroutine ("shakeSmall", 1f);
//												isLaserState = false;

												break;

										case WeaponsController.WEAPONS.MOIRE:

												GameObject bulletM = Instantiate (bulletLaser, transform.position, transform.rotation) as GameObject;
												bulletM.GetComponent<LineRenderer> ().SetWidth (0.01f, 0.1f);
												bulletM.GetComponent<LineRenderer> ().SetPosition (1, new Vector3 (24, 0, 0));
												bulletM.GetComponent<LaserControl> ().timer = 8;
												bulletM.transform.parent = transform;
												Sound.playmoireBulletClip ();

												GameObject raycastO = Instantiate (raycastObj, transform.position, transform.rotation) as GameObject;
												raycastO.GetComponent<RaycastScr> ().timer = 8;
												raycastO.GetComponent<RaycastScr> ().length = 24f;
												raycastO.transform.parent = transform;

												WeaponsController.bullets [(int)WeaponsController.currentWeapon] -= 1;
												shootTime = shotTimeMoire;
												Camera.main.GetComponent<cameraScript> ().StartCoroutine ("shakeSmall", 1f);

												break;

										case WeaponsController.WEAPONS.TWAY:
												for (int i=0; i< 3; i++) {

														Quaternion rotation = transform.rotation;//Quaternion.Euler (transform.rotation.x, transform.rotation.y, transform.rotation.z);
														rotation *= Quaternion.Euler (0, 0, -30);
														rotation *= Quaternion.Euler (0, 0, i * 30);

														Sound.playtriBulletClip ();
														GameObject bullet1 = Instantiate (bullet, transform.position, rotation) as GameObject;
														bullet1.rigidbody.AddForce (bullet1.transform.right * shotForce); 
												}

												WeaponsController.bullets [(int)WeaponsController.currentWeapon] -= 1;
												shootTime = shotTimeTWay;
												Camera.main.GetComponent<cameraScript> ().StartCoroutine ("shakeSmall", 0.3f);

												break;
										case WeaponsController.WEAPONS.CIRCULAR:

												GameObject bulletCircle = Instantiate (cirController, transform.position, transform.rotation) as GameObject;
												bulletCircle.transform.parent = transform;
												Sound.playcircularBulletClip ();


												WeaponsController.bullets [(int)WeaponsController.currentWeapon] -= 1;
												shootTime = shotTimeTWay;
				
												break;

				
										}
								} else {
										//set laser and can move
										if (isLaserState) {
												isLaserState = false;
										}

										gameObject.GetComponentInChildren<Animator> ().Play ("recoil");

										GameObject bulletn = Instantiate (bulletThin, transform.position, transform.rotation) as GameObject;
										bulletn.GetComponent<Rigidbody> ().AddForce (transform.right * shotForce);
										shootTime = shotTimeNormal;
										Sound.playnBulletClip ();


								}
						}
				}





		}

		bool isBullets ()
		{
				bool isBullets = false;

//				if (WeaponsController.bullets [(int)WeaponsController.unlockedWeapons] != null && WeaponsController.bullets [(int)WeaponsController.currentWeapon] != 0) {
				if (WeaponsController.bullets [(int)WeaponsController.currentWeapon] != 0) {

						isBullets = true;
				}

				return isBullets;
		}

}
