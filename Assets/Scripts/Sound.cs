﻿using UnityEngine;
using System.Collections.Generic;


public class Sound : MonoBehaviour
{

		[System.Serializable]
		private class soundLibrary
		{
				//bullets
				public  AudioClip nBulletSnd;
				public  AudioClip wBulletSnd;
				public  AudioClip laserBulletSnd;
				public  AudioClip triBulletSnd;
				public  AudioClip circularBulletSnd;
				public  AudioClip moireBulletSnd;
				//explosions
				public  AudioClip dustXPLSnd;
				public  AudioClip bombaXPLSnd;
				public  AudioClip smallBombaXPLSnd;
				public  AudioClip bombaAmarillaXPLSnd;
				//speech
				public  AudioClip risaSpeechSnd;
				public  AudioClip bolasSpeechSnd;
				public  AudioClip tresViasSpeechSnd;
				public  AudioClip laserSpeechSnd;
				public  AudioClip circularSpeechSnd;
				public  AudioClip repeteaSpeechSnd;
				public  AudioClip rayosSpeechSnd;
				public  AudioClip fantasmaSpeechSnd;
				public  AudioClip bombaSpeechSnd;
				public  AudioClip boomSpeechSnd;
				public  AudioClip caralludoSpeechSnd;
				public  AudioClip crispadoSpeechSnd;
				//Gameplay
				public  AudioClip interferenciaGameSnd;
				public  AudioClip pillaPaqueteGameSnd;
				public  AudioClip buttonShortSnd;
				public  AudioClip buttonLongSnd;


		}


		[SerializeField]
		soundLibrary
				library;


		//bullets
		public static AudioClip nBulletSnd;
		public static   AudioClip wBulletSnd;
		public static   AudioClip laserBulletSnd;
		public static   AudioClip triBulletSnd;
		public  static  AudioClip circularBulletSnd;
		public   static AudioClip moireBulletSnd;
		//explosions
		public   static AudioClip dustXPLSnd;
		public   static AudioClip bombaXPLSnd;
		public   static AudioClip smallBombaXPLSnd;
		public   static AudioClip bombaAmarillaXPLSnd;
		//speech
		public   static AudioClip risaSpeechSnd;
		public   static AudioClip bolasSpeechSnd;
		public   static AudioClip tresViasSpeechSnd;
		public   static AudioClip laserSpeechSnd;
		public   static AudioClip circularSpeechSnd;
		public   static AudioClip repeteaSpeechSnd;
		public   static AudioClip rayosSpeechSnd;
		public   static AudioClip fantasmaSpeechSnd;
		public   static AudioClip bombaSpeechSnd;
		public   static AudioClip boomSpeechSnd;
		public   static AudioClip caralludoSpeechSnd;
		public   static AudioClip crispadoSpeechSnd;
		//Gameplay
		public   static AudioClip interferenciaGameSnd;
		public   static AudioClip pillaPaqueteGameSnd;
		public   static AudioClip buttonShortSnd;
		public  static AudioClip buttonLongSnd;

		public static AudioSource soundPlayer;
		public static float pan;

	
	
	
		void Awake ()
		{
				nBulletSnd = library.nBulletSnd;
				wBulletSnd = library.wBulletSnd;
				laserBulletSnd = library.laserBulletSnd;
				triBulletSnd = library.triBulletSnd;
				circularBulletSnd = library.circularBulletSnd;
				moireBulletSnd = library.moireBulletSnd;

				dustXPLSnd = library.dustXPLSnd;
				bombaXPLSnd = library.bombaXPLSnd;
				smallBombaXPLSnd = library.smallBombaXPLSnd;
				bombaAmarillaXPLSnd = library.bombaAmarillaXPLSnd;

				risaSpeechSnd = library.risaSpeechSnd;
				bolasSpeechSnd = library.bolasSpeechSnd;
				tresViasSpeechSnd = library.tresViasSpeechSnd;
				laserSpeechSnd = library.laserBulletSnd;
				circularSpeechSnd = library.circularBulletSnd;
				repeteaSpeechSnd = library.repeteaSpeechSnd;
				rayosSpeechSnd = library.rayosSpeechSnd;
				fantasmaSpeechSnd = library.fantasmaSpeechSnd;
				bombaSpeechSnd = library.bombaSpeechSnd;
				boomSpeechSnd = library.boomSpeechSnd;
				caralludoSpeechSnd = library.caralludoSpeechSnd;
				crispadoSpeechSnd = library.crispadoSpeechSnd;

				interferenciaGameSnd = library.interferenciaGameSnd;
				pillaPaqueteGameSnd = library.pillaPaqueteGameSnd;
				buttonShortSnd = library.buttonShortSnd;
				buttonLongSnd = library.buttonLongSnd;


				soundPlayer = GetComponent<AudioSource> ();

				print (soundPlayer);
		}

		void Update ()
		{
//				soundPlayer.pitch = Random.Range (0.9f, 1.1f);
//				soundPlayer.pan = (float)gameControl.shipPan;

		}
	

		public static void playnBulletClip ()
		{
				soundPlayer.PlayOneShot (nBulletSnd);

		}
	
		public static void playwBulletClip ()
		{

				soundPlayer.PlayOneShot (wBulletSnd);
		}

		public static void playlaserBulletClip ()
		{
				soundPlayer.PlayOneShot (laserBulletSnd);
		}

		public static void playtriBulletClip ()
		{
				soundPlayer.PlayOneShot (triBulletSnd);
		}

		public static void playcircularBulletClip ()
		{
				soundPlayer.PlayOneShot (circularBulletSnd);
		}

		public static void playmoireBulletClip ()
		{
				soundPlayer.PlayOneShot (moireBulletSnd);
		}

		public static void playdustClip ()
		{
				soundPlayer.PlayOneShot (dustXPLSnd);
		}

		public static void playBombaClip (Vector2 pos)
		{
				soundPlayer.PlayOneShot (bombaXPLSnd);

		}

		public static void playSmallBombaClip (Vector2 pos)
		{
				soundPlayer.PlayOneShot (smallBombaXPLSnd);

		}

		public static void playBombaAmarillaClip (Vector2 pos)
		{
				soundPlayer.PlayOneShot (bombaAmarillaXPLSnd);
		}

		public static void playRisaClip ()
		{
				soundPlayer.PlayOneShot (risaSpeechSnd);

		}
		public static void playBolasClip ()
		{
				soundPlayer.PlayOneShot (bolasSpeechSnd);
		}
		public static void playTresClip ()
		{
				soundPlayer.PlayOneShot (tresViasSpeechSnd);
		}
		public static void playLaserClip ()
		{
				soundPlayer.PlayOneShot (laserSpeechSnd);
		}
		public static void playCircularClip ()
		{
				soundPlayer.PlayOneShot (circularSpeechSnd);
		}
		public static void playRepeteaClip ()
		{
				soundPlayer.PlayOneShot (repeteaSpeechSnd);
		}
		public static void playRayosClip ()
		{
				soundPlayer.PlayOneShot (rayosSpeechSnd);
		}
		public static void playFantasmaClip ()
		{
				soundPlayer.PlayOneShot (fantasmaSpeechSnd);
		}
		public static void playBombaSpeechClip ()
		{
				soundPlayer.PlayOneShot (bombaSpeechSnd);
		}
		public static void playBoomClip ()
		{
				soundPlayer.PlayOneShot (boomSpeechSnd);
		}
		public static void playCaralludoClip ()
		{
				soundPlayer.PlayOneShot (caralludoSpeechSnd);
		}
		public static void playCrispadoClip ()
		{
				soundPlayer.PlayOneShot (crispadoSpeechSnd);
		}
		public static void playInterferenciaClip ()
		{
				if (soundPlayer.clip != interferenciaGameSnd) {

						soundPlayer.PlayOneShot (interferenciaGameSnd);
				}
		}
		public static void playPillarPaqueteClip ()
		{
				soundPlayer.PlayOneShot (pillaPaqueteGameSnd);
		}

		public static void playShortButton ()
		{
		
				soundPlayer.PlayOneShot (buttonShortSnd);

		}

		public static void playLongButton ()
		{
		
				soundPlayer.PlayOneShot (buttonLongSnd);
		
		}
	
}